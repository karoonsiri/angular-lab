Rails.application.routes.draw do
  resources :tasks do
    member do
      patch :finish
      patch :undone
    end
  end

  resources :lists

  resource :landing, only: [] do
    get :hello
    get :kitty
    get :directive
  end

  resource :data_driven, only: [] do
    get :example
  end

  get "/ng_templates/:path" => "ng_templates#page", constraints: { path: /.+/ }

  root to: "landings#hello"
end
