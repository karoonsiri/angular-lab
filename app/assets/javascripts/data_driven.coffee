DataDriven = {
  humanHello: ->
    console.log "#{$(this).data('name')} Hello World"

  dogHello: ->
    console.log "#{$(this).data('name')} Woof Woof"

  firstHello: ->
    console.log "Bicycle!!!!"

  pizzaHello: ->
    console.log "#{$(this).data('topping')} Pizza Hut"

  hello: ->
    type = $(this).data('type')
    $.proxy(DataDriven.typeLogic[type], this)()

  init: ->
    DataDriven.typeLogic = {
      "dog": DataDriven.dogHello,
      "first": DataDriven.firstHello,
      "human": DataDriven.humanHello,
      "pizza": DataDriven.pizzaHello
    }

    $(".hello-btn").on 'click', DataDriven.hello
}

$(DataDriven.init) # init on load
