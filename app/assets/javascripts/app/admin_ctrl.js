adminCtrl = function($scope, ListService) {
  $scope.addList = function() {
    ListService.addList();
  };
};

adminCtrl.$inject = ["$scope", "ListService"];

// Inject a controller into the Todolist module
angular.module('Todolist').controller(
  'AdminController',
  adminCtrl
);