task = ($resource) ->
  return $resource(
    '/tasks/:id/:action.json',
    { id: '@id' },
    {
      update: {
        method: "PATCH"
      },
      finish: {
        method: "PATCH",
        params: {
          action: 'finish'
        }
      },
      undone: {
        method: "PATCH",
        params: {
          action: 'undone'
        }
      }
    }
  )


task.$inject = ['$resource']

angular.module('Todolist').factory 'Task', task