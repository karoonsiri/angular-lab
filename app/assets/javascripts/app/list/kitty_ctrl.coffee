kittyCtrl = ($scope, List, Task) ->
  $scope.addList = ->
    list = new List()
    list.newRecord = true
    $scope.lists.push(list)

  $scope.save = (list) ->
    if list.newRecord
      list.$save()
      list.newRecord = false
    else
      list.$update().then(
        ->
          $scope.getTaskFor(list)
        ,
        (error) ->
          alert "Cannot get save wa"
      )

  $scope.delete = (list) ->
    if list.newRecord
      index = $scope.lists.indexOf(list)
      $scope.lists.splice(index, 1) if index > -1
      return true

    list.$delete().then(
      ->
        index = $scope.lists.indexOf(list)
        $scope.lists.splice(index, 1) if index > -1
      ,
      (error) ->
        alert "Cannot delete na"
    )

  $scope.addTask = (list) ->
    task = new Task()
    task.description = list.newTask

    task.$save(list_id: list.id).then(
      ->
        list.tasks = list.tasks || []
        list.tasks.push(task)
      ,
      (error) ->
        alert "Cannot add task na"
    )
    list.newTask = ''

  $scope.finishTask = (task) ->
    task.$finish()

  $scope.undoneTask = (task) ->
    task.$undone()

  $scope.getTaskFor = (list) ->
    list.tasks = Task.query(list: list.id)

  $scope.init = ->
    $scope.lists = List.query()

    # Trigger after completing getting the lists
    $scope.lists.$promise.then ->
      for list in $scope.lists
        $scope.getTaskFor(list)

  $scope.init()

kittyCtrl.$inject = ['$scope', 'List', 'Task']

angular.module('Todolist').controller 'KittyController', kittyCtrl