listDirective = ->
  return {
    restrict: 'E', # (A)ttribute (E)lement (C)lass
    controller: 'ListController',
    templateUrl: '/ng_templates/list/list.html',
    scope: {
      listId: '@', # @: string, =: expression, &: function
    }
  }

angular.module('Todolist').directive 'todolist', listDirective
