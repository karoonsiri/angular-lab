list = ($resource) ->
  return $resource(
    '/lists/:id/:action.json',
    { id: '@id' },
    {
      update: {
        method: "PATCH"
      }
    }
  )


list.$inject = ['$resource']

angular.module('Todolist').factory 'List', list