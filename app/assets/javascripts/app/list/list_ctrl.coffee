listCtrl = ($scope, List, Task) ->
  getTasks = ->
    $scope.list.$promise.then ->
      $scope.list.tasks = Task.query(list: $scope.list.id)

  $scope.init = ->
    $scope.list = List.get(id: $scope.listId)
    $scope.listTitleEditMode = false
    getTasks()

  $scope.finish = (task) ->
    task.$finish()

  $scope.undone = (task) ->
    task.$undone()

  $scope.addTask = ->
    task = new Task()
    task.description = $scope.taskDescription

    task.$save(list_id: $scope.list.id).then(
      ->
        $scope.list.tasks = $scope.list.tasks || []
        $scope.list.tasks.push(task)
      ,
      (error) ->
        alert "Cannot add task na"
    )

    $scope.taskDescription = ''

  $scope.enableListTitleEditMode = ->
    $scope.listTitleEditMode = true

  $scope.updateListTitle = ->
    $scope.list.$update().then ->
      $scope.listTitleEditMode = false
      getTasks()

  $scope.init()

listCtrl.$inject = ['$scope', 'List', 'Task']

angular.module('Todolist').controller 'ListController', listCtrl
