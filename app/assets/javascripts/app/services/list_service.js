listService = function() {
  var newList = function() {
    return {
      name: '',
      dont: false
    }
  };

  var addList = function() {
    _service.lists.push(newList());
  };

  var _service = {
    lists: [],
    addList: addList
  };

  return _service;
};

angular.module('Todolist').service('ListService', listService)