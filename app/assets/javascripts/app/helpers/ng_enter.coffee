ngEnter = ->
  linkFn = (scope, element, attrs) ->
    element.bind 'keydown keypress', (event) ->
      if event.which is 13
        scope.$apply ->
          scope.$eval attrs.ngEnter

        event.preventDefault()

  return {
    restrict: 'A',
    link: linkFn
  }

angular.module('Todolist').directive 'ngEnter', ngEnter
