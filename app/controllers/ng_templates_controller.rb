class NgTemplatesController < ApplicationController
  before_action :set_path

  def page
    begin
      render template: "ng_templates/#{@path}", layout: false
    rescue
      render_not_found and return false
    end
  end

  private

  def set_path
    @path = params[:path]
    render_not_found and return false if @path.blank?
    @path.sub!(/\/+$/, '')
  end
end
